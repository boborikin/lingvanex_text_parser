# Lingvanex test task
Тестовое задание на должность Python Developer.
## Running code

```bash
python main.py
```



## Installing requirements

```bash
pip install -r requirements.txt
```
## Running tests

```bash
pytest ./tests
```
