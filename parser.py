from typing import List


class Parser:
    """This class is designed to parse text file"""

    @staticmethod
    def _read_data_from_source(source: str) -> List[str]:
        with open(source, "r", encoding="UTF-8") as file:
            data = list(file.readlines())
        return data

    @staticmethod
    def _write_words_to_file(filename: str, words_list: List[str]):
        with open(filename, "a", encoding="UTF-8") as file:
            for word in words_list:
                file.write(word.strip()+"\n")

    def _write_first_language_words(self, data: List[str], filename: str):
        for item in data:
            words = item.split("\t")[0].split(";")
            for _ in range(len(item.split("\t")[1].split(";"))):
                self._write_words_to_file(filename, words)

    def _write_second_language_words(self, data: List[str], filename: str):
        for item in data:
            words = item.split("\t")[1].split(";")
            for _ in range(len(item.split("\t")[0].split(";"))):
                self._write_words_to_file(filename, words)

    def parse(self, source_file: str, first_filename: str, second_filename: str):
        """
        This function parses source file and returns two files:
           English.txt and Russian.txt
        """
        data = self._read_data_from_source(source_file)
        self._write_first_language_words(data, first_filename)
        self._write_second_language_words(data, second_filename)
