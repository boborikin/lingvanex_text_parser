from parser import Parser

pars = Parser()


def create_test_data(test_data):
    with open("tests/testfile.txt", "w") as f_o:
        f_o.writelines(test_data)


def test_read_data_from_source():
    test_data = ['Hello']
    create_test_data(test_data)
    data = pars._read_data_from_source("tests/testfile.txt")
    assert data == ['Hello']


def test_write_words_to_file():
    words = ["Hello", "World"]
    pars._write_words_to_file("tests/testfile.txt", words)
    with open("tests/testfile.txt", "r") as f:
        data = f.readlines()
    test_data = ["Hello\n", "World\n"]
    assert test_data == data



